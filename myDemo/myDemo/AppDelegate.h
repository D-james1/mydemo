//
//  AppDelegate.h
//  myDemo
//
//  Created by duanshengwu on 2018/10/23.
//  Copyright © 2018年 D-James. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

