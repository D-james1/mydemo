//
//  main.m
//  myDemo
//
//  Created by duanshengwu on 2018/10/23.
//  Copyright © 2018年 D-James. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
